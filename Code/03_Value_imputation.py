import argparse
import pandas as pd
import numpy as np


data= pd.read_csv("")
# carrying over the values to a new column called lems
data['lems_q6'] = data['c_lems'].fillna(data["aiii_lems"])
data['lems_q6_q3'] = data['lems_q6'].fillna(data["aii_lems"])

# carrying over the asia score to a new column called asia_chronic
#"va_ais", "ai_ais", "aii_aiis", "aiii_aiiis", "c_cs"
data['asia_chronic_q6'] = data['c_cs'].fillna(data["aiii_aiiis"])
data['asia_chronic_q6_q3'] = data['asia_chronic_q6'].fillna(data["aii_aiis"])


# index for patients who have all 0 values
all_0_index=[] # insert 0

data["va_lems_imputed"]=data["va_lems"]
for i in range(len(all_0_index)):
    # create a new columns with the imputed values
    data.at[all_0_index[i],"va_lems_imputed"]= 0

# removed due to abnormal decrease in LEMS score
df=data.drop(index=[])

df.to_csv("")